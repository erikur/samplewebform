﻿Public Class SampleKatReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblKet.Text = DateTime.Now.ToString()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        ReportViewer1.LocalReport.Refresh()
    End Sub
End Class