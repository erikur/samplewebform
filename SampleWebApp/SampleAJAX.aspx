﻿<%@ Page Title="Sample AJAX" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleAJAX.aspx.vb" Inherits="SampleWebApp.SampleAJAX" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Sample AJAX</li>
    </ol>
    <div class="row">
        <div class="col-6">
            <asp:Label ID="lblDateTime1" runat="server" />
            <asp:Button Text="Submit" ID="btnSubmit1" runat="server" />
            <hr />
            <asp:UpdatePanel ID="up1" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSubmit1" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <asp:Label ID="lblDateTime2" runat="server" />
                    <asp:Button Text="Submit" ID="btnSubmit2" runat="server" />

                    <asp:SqlDataSource runat="server" ID="sdsKategori" 
                        ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>" SelectCommand="SELECT * FROM [Kategori]" />
                    <asp:GridView runat="server" ID="gvKategori" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="KategoriID" DataSourceID="sdsKategori">
                        <Columns>
                            <asp:BoundField DataField="KategoriID" HeaderText="KategoriID" InsertVisible="False" ReadOnly="True" SortExpression="KategoriID" />
                            <asp:BoundField DataField="NamaKategori" HeaderText="NamaKategori" SortExpression="NamaKategori" />
                            <asp:BoundField DataField="Count" HeaderText="Count" SortExpression="Count" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
