﻿Public Class SampleTimer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim listData As New List(Of String) From {
            "Programming ASP.NET", "Belajar VB.NET", "Belajar Xamarin Forms",
            "Programming PHP", "Web Development with Blazor"}
        Dim rnd As New Random
        lblInformasi.Text += listData(rnd.Next(listData.Count)) & "<br/>"
    End Sub

End Class