﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SetupKategoriForm.aspx.vb" Inherits="SampleWebApp.SetupKategoriForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource ID="sdsKategori" runat="server"
        ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>"
        DeleteCommand="DELETE FROM [Kategori] WHERE [KategoriID] = @KategoriID"
        InsertCommand="INSERT INTO [Kategori] ([NamaKategori], [Count]) VALUES (@NamaKategori, @Count)" SelectCommand="SELECT [KategoriID], [NamaKategori], [Count] FROM [Kategori]" UpdateCommand="UPDATE [Kategori] SET [NamaKategori] = @NamaKategori, [Count] = @Count WHERE [KategoriID] = @KategoriID">
        <DeleteParameters>
            <asp:Parameter Name="KategoriID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="NamaKategori" Type="String" />
            <asp:Parameter Name="Count" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="NamaKategori" Type="String" />
            <asp:Parameter Name="Count" Type="Int32" />
            <asp:Parameter Name="KategoriID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Tambah Kategori</li>
    </ol>
    <div class="row">
        <div class="col-6">
            <asp:FormView runat="server" DataKeyNames="KategoriID" DataSourceID="sdsKategori" DefaultMode="Insert" OnItemInserted="Unnamed1_ItemInserted">
                <InsertItemTemplate>
                    <div class="form-group">
                        <label>Nama Kategori:</label>
                        <asp:TextBox ID="NamaKategoriTextBox" CssClass="form-control" runat="server" Text='<%# Bind("NamaKategori") %>' />
                    </div>
                    <div class="form-group">
                        <label>Count:</label>
                        <asp:TextBox ID="CountTextBox" runat="server" Text='<%# Bind("Count") %>' CssClass="form-control" />
                    </div>
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CssClass="btn btn-success" CommandName="Insert" Text="Insert" />
                </InsertItemTemplate>
            </asp:FormView>
            <asp:Literal ID="ltKeterangan" runat="server" />
        </div>
    </div>


</asp:Content>
