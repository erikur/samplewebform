﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleListView.aspx.vb" Inherits="SampleWebApp.SampleListView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Sample List View</li>
    </ol>
    <div class="row">

        <asp:ListView ID="lvKategori" ItemType="SampleWebApp.Kategori"
            SelectMethod="lvKategori_GetData"  runat="server">
            <ItemTemplate>
                <div class="col-md-3" style="border: 1px solid red">
                    <label><%# Eval("KategoriID") %></label>
                    <p><%# Eval("NamaKategori") %></p>
                </div>
            </ItemTemplate>
        </asp:ListView>


        <div class="col-md-6">
            <asp:ListView ID="lvKategoriTable" ItemType="SampleWebApp.Kategori"
                SelectMethod="lvKategori_GetData" InsertMethod="lvKategoriTable_InsertItem" InsertItemPosition="FirstItem" runat="server">
                <LayoutTemplate>
                    <table class="table table-striped">
                        <tr>
                            <th>Kategori ID</th>
                            <th>Nama</th>
                        </tr>
                        <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("KategoriID") %></td>
                        <td><%# Eval("NamaKategori") %></td>
                    </tr>
                </ItemTemplate>
                <InsertItemTemplate>
                    <div class="form-group">
                        <label>Nama Kategori :</label>
                        <asp:TextBox runat="server" ID="txtNamaKategori" CssClass="form-control"
                            Text='<%# BindItem.NamaKategori %>' />
                        <asp:Button Text="Tambah" CssClass="btn btn-success" CommandName="Insert" runat="server" />
                    </div>
                </InsertItemTemplate>
            </asp:ListView>
        </div>


    </div>

</asp:Content>
