﻿<%@ Page Title="Progress Bar" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleProgress.aspx.vb" Inherits="SampleWebApp.SampleProgress" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <%= DateTime.Now.ToString("T") %>
            <asp:Button Text="Submit" ID="btnSubmit" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div>
                <asp:Image ImageUrl="~/Images/progress.gif" runat="server" />
                Loading Data ....
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
