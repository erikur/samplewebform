﻿Public Class SetupKategoriPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

        End If
    End Sub

    Protected Sub btnTambah_Click(sender As Object, e As EventArgs) Handles btnTambah.Click
        Try
            sdsKategori.InsertParameters("NamaKategori").DefaultValue =
                txtNamaKategori.Text
            sdsKategori.Insert()
            lblKeterangan.Text = "Data Kategori berhasil ditambahkan !"
        Catch ex As Exception
            lblKeterangan.Text = "Error :" & ex.Message
        End Try
    End Sub

    Protected Sub sdsKategori_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles sdsKategori.Updating
        For Each par As System.Data.SqlClient.SqlParameter In e.Command.Parameters
            If par.Value Is Nothing Then
                e.Cancel = True
                lblKeterangan.Text = "Field " + par.ParameterName + " harus diisi"
            End If
        Next
    End Sub

    'search untuk roundtrip ke server
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        'sdsKategori.SelectCommand = String.Format("select * from Kategori where NamaKategori like '%{0}%'", txtSearch.Text)
        'sdsKategori.DataBind()
        'gvKategori.DataBind()
        If rdKode.Checked Then
            sdsFilter.FilterExpression = "KategoriID = {0}"
        Else
            sdsFilter.FilterExpression = "NamaKategori like '%{0}%'"
        End If

    End Sub

    'Protected Sub lvTambahKategori_ItemInserted(sender As Object, e As ListViewInsertedEventArgs) Handles lvTambahKategori.ItemInserted
    '    sdsKategori.DataBind()
    'End Sub
End Class