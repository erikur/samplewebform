﻿Public Class Registrasi
    Inherits System.Web.UI.Page

    Private penggunaDal As New PenggunaDAL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim newPengguna As New Pengguna With {
                .Username = txtUsername.Text,
                .Password = txtPassword.Text,
                .AksesLevel = CInt(ddAksesLevel.SelectedValue)}
            penggunaDal.Registrasi(newPengguna)
            ltKeterangan.Text = $"<span class='alert alert-success'>Registrasi Berhasil !</span>"
        Catch ex As Exception
            ltKeterangan.Text = $"<span class='alert alert-danger'>{ex.Message}</span>"
        End Try
    End Sub
End Class