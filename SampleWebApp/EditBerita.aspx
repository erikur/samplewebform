﻿<%@ Page Title="Edit Berita" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="EditBerita.aspx.vb" Inherits="SampleWebApp.EditBerita" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Edit Berita</li>
    </ol>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="txtBeritaID">Berita ID :</label>
                <asp:TextBox runat="server" ID="txtBeritaID" ReadOnly="true" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label>Nama Kategori :</label>
                <asp:DropDownList runat="server" ID="ddKategori" CssClass="form-control"
                    DataTextField="NamaKategori" DataValueField="KategoriID">
                </asp:DropDownList>
            </div>
            <div class="form-group">
                <label for="txtJudul">Judul :</label>
                <asp:TextBox runat="server" ID="txtJudul" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="txtDetail">Detail :</label>
                <asp:TextBox runat="server" ID="txtDetail" TextMode="MultiLine" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="txtTanggal">Tanggal :</label>
                <asp:TextBox runat="server" ID="txtTanggal" TextMode="Date" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="fpGambar">Gambar :</label>
                <asp:FileUpload ID="fpGambar" CssClass="form-control" runat="server" />
            </div>
            <div class="form-group">
                <asp:Image ID="Gambar" Width="120" runat="server" />
            </div>
            <asp:Button Text="Edit" ID="btnEdit" runat="server" CssClass="btn btn-primary" />
            <hr />
            <asp:Literal ID="ltKeterangan" runat="server" />

        </div>
    </div>
</asp:Content>
