﻿<%@ Page Title="Sample Data Source" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleSQLDataSource.aspx.vb" Inherits="SampleWebApp.SampleSQLDataSource" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource ID="sdsData" runat="server" 
        ConnectionString="<%$ ConnectionStrings:MyConnectionString %>" 
        SelectCommand="SELECT * FROM [Biodata] ORDER BY [Nama]">
    </asp:SqlDataSource>

    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="PersonID" DataSourceID="sdsData" CssClass="table table-striped">
        <Columns>
            <asp:BoundField DataField="PersonID" HeaderText="PersonID" InsertVisible="False" ReadOnly="True" SortExpression="PersonID" />
            <asp:BoundField DataField="Nama" HeaderText="Nama" SortExpression="Nama" />
            <asp:BoundField DataField="Alamat" HeaderText="Alamat" SortExpression="Alamat" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Telp" HeaderText="Telp" SortExpression="Telp" />
            <asp:BoundField DataField="Umur" HeaderText="Umur" SortExpression="Umur" />
        </Columns>
    </asp:GridView>
</asp:Content>
