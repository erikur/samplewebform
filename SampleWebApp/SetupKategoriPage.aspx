﻿<%@ Page Title="Setup Kategori" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SetupKategoriPage.aspx.vb" Inherits="SampleWebApp.SetupKategoriPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Setup Kategori</li>
    </ol>
    <div class="row">
        <div class="col-6">
            <asp:SqlDataSource ID="sdsFilter" runat="server"
                ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>"
                SelectCommand="SELECT [KategoriID], [NamaKategori] FROM [Kategori] ORDER BY [NamaKategori] DESC"
                DeleteCommand="DELETE FROM [Kategori] WHERE [KategoriID] = @KategoriID"
                InsertCommand="INSERT INTO [Kategori] ([NamaKategori]) VALUES (@NamaKategori)"
                UpdateCommand="UPDATE [Kategori] SET [NamaKategori] = @NamaKategori WHERE [KategoriID] = @KategoriID"
                FilterExpression="NamaKategori like '%{0}%'">
                <DeleteParameters>
                    <asp:Parameter Name="KategoriID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="NamaKategori" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="NamaKategori" Type="String" />
                    <asp:Parameter Name="KategoriID" Type="Int32" />
                </UpdateParameters>
                <FilterParameters>
                    <asp:ControlParameter Name="NamaKategori" ControlID="txtSearch" />
                </FilterParameters>
            </asp:SqlDataSource>

            <asp:SqlDataSource ID="sdsKategori" runat="server"
                ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>"
                SelectCommand="SELECT [KategoriID], [NamaKategori] FROM [Kategori] WHERE ([NamaKategori] 
                LIKE '%' + @NamaKategori + '%') ORDER BY [NamaKategori] DESC"
                DeleteCommand="DELETE FROM [Kategori] WHERE [KategoriID] = @KategoriID"
                InsertCommand="INSERT INTO [Kategori] ([NamaKategori]) VALUES (@NamaKategori)"
                UpdateCommand="UPDATE [Kategori] SET [NamaKategori] = @NamaKategori WHERE [KategoriID] = @KategoriID">
                <DeleteParameters>
                    <asp:Parameter Name="KategoriID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="NamaKategori" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtSearch" Name="NamaKategori" PropertyName="Text" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="NamaKategori" Type="String" />
                    <asp:Parameter Name="KategoriID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

           <%-- <asp:SqlDataSource runat="server" ID="sdsKategori" ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>" SelectCommand="SELECT [KategoriID], [NamaKategori] FROM [Kategori]" />--%>

            <%-- <asp:ListView ID="lvTambahKategori" runat="server" 
                DataSourceID="sdsKategori" InsertItemPosition="FirstItem">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate></ItemTemplate>
                <InsertItemTemplate>
                    <div class="form-group">
                        <label for="txtNamaKategori">Masukan Nama Kategori :</label>
                        <asp:TextBox runat="server" ID="txtNamaKategori" 
                            Text='<%# Bind("NamaKategori") %>'  />
                        <asp:LinkButton Text="Insert" CssClass="btn btn-success" 
                            runat="server" ID="btnInsert" CommandName="Insert" />
                    </div>
                </InsertItemTemplate>
            </asp:ListView>--%>
            <div class="form-group">
                <label for="txtNamaKategori">Masukan Nama Kategori :</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtNamaKategori" />
            </div>
            <asp:Button ID="btnTambah" Text="Tambah" CssClass="btn btn-success btn-sm"
                runat="server" /><br />
            <br />
            <asp:Label ID="lblKeterangan" runat="server" />
            <hr />

            <div class="form-inline">
                <div class="form-group">
                    <label for="txtSearch">Search :</label>
                    <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" />
                </div>
                <div class="form-group">
                    <asp:RadioButton ID="rdKode" GroupName="search" Text="Kode" runat="server" />&nbsp;
                    <asp:RadioButton ID="rdNama" GroupName="search" Text="Nama" runat="server" />
                </div>
                <asp:Button Text="Search" ID="btnSearch" CssClass="btn btn-default btn-sm" runat="server" />
            </div>
            <br />

            <asp:GridView ID="gvKategori" runat="server" AutoGenerateColumns="False"
                DataKeyNames="KategoriID" CssClass="table table-striped" DataSourceID="sdsFilter"
                AllowPaging="True" AllowSorting="True">
                <Columns>
                    <asp:BoundField DataField="KategoriID" HeaderText="KategoriID"
                        InsertVisible="False" ReadOnly="True" SortExpression="KategoriID" />
                    <asp:BoundField DataField="NamaKategori" HeaderText="NamaKategori"
                        SortExpression="NamaKategori" />
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:Button ID="btnEdit" runat="server"
                                CommandName="Edit" Text="Edit" CssClass="btn btn-warning btn-sm" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btnUpdate" runat="server"
                                CommandName="Update" Text="Update" CssClass="btn btn-success btn-sm" />
                            <asp:Button ID="btnCancel" runat="server"
                                CommandName="Cancel" Text="Cancel" CssClass="btn btn-default btn-sm" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:Button ID="btnDelete" runat="server"
                                CommandName="Delete" Text="Delete" CssClass="btn btn-danger btn-sm"
                                OnClientClick="return confirm('Are you sure you want to delete this user?');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

          <%--  <asp:GridView ID="gvKategori" runat="server" AutoGenerateColumns="False"
                DataKeyNames="KategoriID" DataSourceID="sdsKategori">
                <Columns>
                    <asp:BoundField DataField="KategoriID" HeaderText="KategoriID" InsertVisible="False" ReadOnly="True" SortExpression="KategoriID" />
                    <asp:BoundField DataField="NamaKategori" HeaderText="NamaKategori" SortExpression="NamaKategori" />
                </Columns>
            </asp:GridView>--%>
        </div>
    </div>
</asp:Content>
