﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportKategori.aspx.vb" Inherits="SampleWebApp.ReportKategori" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Kategori</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <label>Masukan Nama Kategori :</label>
            <asp:TextBox runat="server" ID="txtNamaKategori" />
            <asp:Button Text="Search" ID="btnSearch" runat="server" />

            <asp:SqlDataSource runat="server" ID="sdsKategori" 
                ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>" 
                SelectCommand="SELECT * FROM [Kategori] ORDER BY [NamaKategori] DESC"
                FilterExpression="NamaKategori like '%{0}%'">
                <FilterParameters>
                    <asp:ControlParameter ControlID="txtNamaKategori" Name="NamaKategori" />
                </FilterParameters>
            </asp:SqlDataSource>
            <rsweb:ReportViewer ID="rvKategori" runat="server" BackColor="" ClientIDMode="AutoID" Height="600px" HighlightBackgroundColor="" InternalBorderColor="204, 204, 204" InternalBorderStyle="Solid" InternalBorderWidth="1px" LinkActiveColor="" LinkActiveHoverColor="" LinkDisabledColor="" PrimaryButtonBackgroundColor="" PrimaryButtonForegroundColor="" PrimaryButtonHoverBackgroundColor="" PrimaryButtonHoverForegroundColor="" SecondaryButtonBackgroundColor="" SecondaryButtonForegroundColor="" SecondaryButtonHoverBackgroundColor="" SecondaryButtonHoverForegroundColor="" SplitterBackColor="" ToolbarDividerColor="" ToolbarForegroundColor="" ToolbarForegroundDisabledColor="" ToolbarHoverBackgroundColor="" ToolbarHoverForegroundColor="" ToolBarItemBorderColor="" ToolBarItemBorderStyle="Solid" ToolBarItemBorderWidth="1px" ToolBarItemHoverBackColor="" ToolBarItemPressedBorderColor="51, 102, 153" ToolBarItemPressedBorderStyle="Solid" ToolBarItemPressedBorderWidth="1px" ToolBarItemPressedHoverBackColor="153, 187, 226" Width="800px">
                <LocalReport ReportPath="Report\ReportKategori.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="sdsKategori" Name="dsKategori" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
        </div>
    </form>
</body>
</html>
