﻿Public Class SampleListView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private kategoriDAL As New KategoriDAL
    Public Function lvKategori_GetData() As IEnumerable(Of SampleWebApp.Kategori)
        Return kategoriDAL.GetAll()
    End Function


    Public Sub lvKategoriTable_InsertItem()
        Dim item = New SampleWebApp.Kategori()
        TryUpdateModel(item)
        If ModelState.IsValid Then
            kategoriDAL.Insert(item)
        End If
    End Sub
End Class