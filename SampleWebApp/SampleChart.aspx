﻿<%@ Page Title="Sample Chart" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleChart.aspx.vb" Inherits="SampleWebApp.SampleChart" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" ID="sdsKategori" ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>" SelectCommand="SELECT [Jumlah], [NamaKategori] FROM [ViewBeritaCount]" />
    <asp:Chart ID="Chart1" runat="server" DataSourceID="sdsKategori">
        <Series>
            <asp:Series Name="Series1" XValueMember="NamaKategori" YValueMembers="Jumlah"></asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</asp:Content>

