﻿<%@ Page Title="Contoh Upload" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="ContohUpload.aspx.vb" Inherits="SampleWebApp.ContohUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Contoh Upload</li>
    </ol>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="fpUpload">Masukan Gambar :</label>
                <asp:FileUpload ID="fpUpload" runat="server" />
            </div>
            <asp:Button Text="Submit" ID="btnUpload" runat="server" /><br /><br />
            <asp:Label ID="lblKeterangan" runat="server" />
        </div>
    </div>
</asp:Content>
