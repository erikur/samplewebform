﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SampleKatReport.aspx.vb" Inherits="SampleWebApp.SampleKatReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Kategori</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:Label Text="" ID="lblKet" runat="server" />
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                     <label>Nama Kategori :</label>
            <asp:TextBox runat="server" ID="txtSearch" />
            <asp:Button Text="Search" ID="btnSearch" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            
           

            <asp:SqlDataSource runat="server" ID="sdsKategori" ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>" 
                SelectCommand="SELECT [KategoriID], [NamaKategori], [Count] FROM [Kategori]"
                FilterExpression="NamaKategori like '%{0}%'">
                <FilterParameters>
                    <asp:ControlParameter ControlID="txtSearch" Name="NamaKategori" />
                </FilterParameters>
            </asp:SqlDataSource>

            <rsweb:ReportViewer ID="ReportViewer1" runat="server" BackColor="" ClientIDMode="AutoID" HighlightBackgroundColor="" InternalBorderColor="204, 204, 204" InternalBorderStyle="Solid" InternalBorderWidth="1px" LinkActiveColor="" LinkActiveHoverColor="" LinkDisabledColor="" PrimaryButtonBackgroundColor="" PrimaryButtonForegroundColor="" PrimaryButtonHoverBackgroundColor="" PrimaryButtonHoverForegroundColor="" SecondaryButtonBackgroundColor="" SecondaryButtonForegroundColor="" SecondaryButtonHoverBackgroundColor="" SecondaryButtonHoverForegroundColor="" SplitterBackColor="" ToolbarDividerColor="" ToolbarForegroundColor="" ToolbarForegroundDisabledColor="" ToolbarHoverBackgroundColor="" ToolbarHoverForegroundColor="" ToolBarItemBorderColor="" ToolBarItemBorderStyle="Solid" ToolBarItemBorderWidth="1px" ToolBarItemHoverBackColor="" ToolBarItemPressedBorderColor="51, 102, 153" ToolBarItemPressedBorderStyle="Solid" ToolBarItemPressedBorderWidth="1px" ToolBarItemPressedHoverBackColor="153, 187, 226" Width="800px">
                <LocalReport ReportPath="Report\ReportKat.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="sdsKategori" Name="dsKategori" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>

        </div>

    </form>
</body>
</html>
