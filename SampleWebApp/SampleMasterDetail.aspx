﻿<%@ Page Title="Master Page" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleMasterDetail.aspx.vb" Inherits="SampleWebApp.SampleMasterDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" ID="sdsKategori"
        ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>"
        SelectCommand="SELECT [KategoriID], [NamaKategori] FROM [Kategori] ORDER BY [NamaKategori] DESC" />
    <asp:SqlDataSource runat="server" ID="sdsBerita" 
        ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>" 
        SelectCommand="SELECT * FROM [ViewBeritaWithKategori] WHERE ([KategoriID] = @KategoriID)" >
        <SelectParameters>
            <asp:ControlParameter ControlID="ddKategori" Name="KategoriID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Master Detail</li>
    </ol>
    <div class="row">
        <div class="col-6">
            <asp:DropDownList runat="server" ID="ddKategori" AutoPostBack="true"
                DataSourceID="sdsKategori" DataTextField="NamaKategori" DataValueField="KategoriID">
            </asp:DropDownList>
            <hr />
            <asp:GridView runat="server" ID="gvKategori" AutoGenerateColumns="False" 
                DataKeyNames="KategoriID" DataSourceID="sdsKategori">
                <Columns>
                    <asp:BoundField DataField="KategoriID" HeaderText="KategoriID" InsertVisible="False" ReadOnly="True" SortExpression="KategoriID" />
                    <asp:BoundField DataField="NamaKategori" HeaderText="NamaKategori" SortExpression="NamaKategori" />
                    <asp:CommandField ShowSelectButton="True" />
                </Columns>
            </asp:GridView>
            <hr />
            <br /><br />
            <asp:GridView runat="server" ID="gvBerita" CssClass="table table-striped"
                AutoGenerateColumns="False" DataKeyNames="BeritaID" DataSourceID="sdsBerita">
                <Columns>
                    <asp:ImageField DataImageUrlField="Gambar" DataImageUrlFormatString="~/Images/{0}">
                        <ControlStyle Width="80px" />
                    </asp:ImageField>
                    <asp:BoundField DataField="BeritaID" HeaderText="BeritaID" InsertVisible="False" ReadOnly="True" SortExpression="BeritaID" />
                    <asp:BoundField DataField="NamaKategori" HeaderText="Nama Kategori" SortExpression="KategoriID" />
                    <asp:BoundField DataField="JudulBerita" HeaderText="JudulBerita" SortExpression="JudulBerita" />
                    <asp:BoundField DataField="DetailBerita" HeaderText="DetailBerita" SortExpression="DetailBerita" />
                    <asp:BoundField DataField="Tanggal" HeaderText="Tanggal" SortExpression="Tanggal" />
                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>
