USE [master]
GO
/****** Object:  Database [SampleASPDb]    Script Date: 7/19/2018 8:14:27 AM ******/
CREATE DATABASE [SampleASPDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SampleASPDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\SampleASPDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SampleASPDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\SampleASPDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SampleASPDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SampleASPDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SampleASPDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SampleASPDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SampleASPDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SampleASPDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [SampleASPDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SampleASPDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SampleASPDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SampleASPDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SampleASPDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SampleASPDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SampleASPDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SampleASPDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SampleASPDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SampleASPDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SampleASPDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SampleASPDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SampleASPDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SampleASPDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SampleASPDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SampleASPDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SampleASPDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SampleASPDb] SET RECOVERY FULL 
GO
ALTER DATABASE [SampleASPDb] SET  MULTI_USER 
GO
ALTER DATABASE [SampleASPDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SampleASPDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SampleASPDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SampleASPDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SampleASPDb', N'ON'
GO
USE [SampleASPDb]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [SampleASPDb]
GO
/****** Object:  Table [dbo].[Berita]    Script Date: 7/19/2018 8:14:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Berita](
	[BeritaID] [int] IDENTITY(1,1) NOT NULL,
	[KategoriID] [int] NOT NULL,
	[JudulBerita] [varchar](max) NULL,
	[DetailBerita] [varchar](max) NULL,
	[Tanggal] [date] NOT NULL,
	[Gambar] [varchar](50) NULL,
 CONSTRAINT [PK_Berita] PRIMARY KEY CLUSTERED 
(
	[BeritaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kategori]    Script Date: 7/19/2018 8:14:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kategori](
	[KategoriID] [int] IDENTITY(1,1) NOT NULL,
	[NamaKategori] [varchar](50) NULL,
	[Count] [int] NULL,
 CONSTRAINT [PK_Kategori] PRIMARY KEY CLUSTERED 
(
	[KategoriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pengguna]    Script Date: 7/19/2018 8:14:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pengguna](
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[AksesLevel] [int] NOT NULL,
 CONSTRAINT [PK_Pengguna] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ViewBeritaCount]    Script Date: 7/19/2018 8:14:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewBeritaCount]
AS
SELECT        COUNT(dbo.Berita.KategoriID) AS Jumlah, dbo.Kategori.NamaKategori
FROM            dbo.Berita INNER JOIN
                         dbo.Kategori ON dbo.Berita.KategoriID = dbo.Kategori.KategoriID
GROUP BY dbo.Kategori.KategoriID, dbo.Kategori.NamaKategori

GO
/****** Object:  View [dbo].[ViewBeritaWithKategori]    Script Date: 7/19/2018 8:14:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewBeritaWithKategori]
AS
SELECT        dbo.Berita.BeritaID, dbo.Berita.KategoriID, dbo.Kategori.NamaKategori, dbo.Berita.JudulBerita, dbo.Berita.DetailBerita, dbo.Berita.Tanggal, dbo.Berita.Gambar
FROM            dbo.Berita INNER JOIN
                         dbo.Kategori ON dbo.Berita.KategoriID = dbo.Kategori.KategoriID

GO
SET IDENTITY_INSERT [dbo].[Berita] ON 

INSERT [dbo].[Berita] ([BeritaID], [KategoriID], [JudulBerita], [DetailBerita], [Tanggal], [Gambar]) VALUES (1, 1, N'Belajar CSharp', N'Belajar CSharp Fundamental', CAST(N'2018-07-17' AS Date), NULL)
INSERT [dbo].[Berita] ([BeritaID], [KategoriID], [JudulBerita], [DetailBerita], [Tanggal], [Gambar]) VALUES (2, 2, N'Belajar VB', N'Belajar VB Fundamental', CAST(N'2018-07-17' AS Date), NULL)
INSERT [dbo].[Berita] ([BeritaID], [KategoriID], [JudulBerita], [DetailBerita], [Tanggal], [Gambar]) VALUES (3, 2, N'VB.NET Advance', N'Advance VB NET ', CAST(N'2018-07-17' AS Date), N'3df865acnasgor.jpg')
INSERT [dbo].[Berita] ([BeritaID], [KategoriID], [JudulBerita], [DetailBerita], [Tanggal], [Gambar]) VALUES (4, 9, N'PHP with Control Toolkit', N'PHP Control Toolkit', CAST(N'2018-07-18' AS Date), N'b906ada5sateklathak.jpg')
SET IDENTITY_INSERT [dbo].[Berita] OFF
SET IDENTITY_INSERT [dbo].[Kategori] ON 

INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (1, N'CSharp', 10)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (2, N'VB.NET', 18)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (3, N'Java', 20)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (4, N'Phyton', 15)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (5, N'Ruby', 20)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (6, N'Go', 11)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (7, N'Erlang', 15)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (9, N'PHP', 18)
INSERT [dbo].[Kategori] ([KategoriID], [NamaKategori], [Count]) VALUES (14, N'AJAX', 15)
SET IDENTITY_INSERT [dbo].[Kategori] OFF
INSERT [dbo].[Pengguna] ([Username], [Password], [AksesLevel]) VALUES (N'budi', N'AC43724F16E9241D990427AB7C8F4228', 0)
INSERT [dbo].[Pengguna] ([Username], [Password], [AksesLevel]) VALUES (N'erick', N'AC43724F16E9241D990427AB7C8F4228', 1)
SET ANSI_PADDING ON

GO
/****** Object:  Index [Unique_Kategori]    Script Date: 7/19/2018 8:14:27 AM ******/
ALTER TABLE [dbo].[Kategori] ADD  CONSTRAINT [Unique_Kategori] UNIQUE NONCLUSTERED 
(
	[NamaKategori] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Berita] ADD  CONSTRAINT [DF_Berita_Tanggal]  DEFAULT (getdate()) FOR [Tanggal]
GO
ALTER TABLE [dbo].[Berita]  WITH CHECK ADD  CONSTRAINT [FK_Berita_Kategori] FOREIGN KEY([KategoriID])
REFERENCES [dbo].[Kategori] ([KategoriID])
GO
ALTER TABLE [dbo].[Berita] CHECK CONSTRAINT [FK_Berita_Kategori]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[12] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Berita"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Kategori"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 119
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewBeritaCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewBeritaCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Berita"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 185
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Kategori"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 170
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewBeritaWithKategori'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewBeritaWithKategori'
GO
USE [master]
GO
ALTER DATABASE [SampleASPDb] SET  READ_WRITE 
GO
