﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/StartBootstrap.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="SampleWebApp._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Home Page</li>
    </ol>
    <div class="row">
        <div class="col-12">
            <h1>Home Page</h1>
            <p>This is an example of a blank page that you can use as a starting point for creating new ones.</p>
        </div>
    </div>
</asp:Content>
