﻿Public Class Login
    Inherits System.Web.UI.Page

    Private penggunaDal As New PenggunaDAL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try

            Dim pengguna = penggunaDal.Login(txtUsername.Text, txtPassword.Text)
            If Not pengguna Is Nothing Then
                Session("username") = pengguna.Username
                Session("akseslevel") = pengguna.AksesLevel
                Response.Redirect("~/SetupBerita.aspx")
            Else
                ltKeterangan.Text = $"<span class='alert alert-warning'>Username/Password tidak ditemukan</span>"
            End If
        Catch ex As Exception
            ltKeterangan.Text = $"<span class='alert alert-danger'>{ex.Message}</span>"
        End Try
    End Sub
End Class