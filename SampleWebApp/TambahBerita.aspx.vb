﻿Imports System.IO

Public Class TambahBerita
    Inherits System.Web.UI.Page

    Private _beritaDAL As BeritaDAL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _beritaDAL = New BeritaDAL()
    End Sub




    Protected Sub btnTambah_Click(sender As Object, e As EventArgs) Handles btnTambah.Click
        Try
            If fpGambar.HasFile Then
                Dim strGuid = Guid.NewGuid.ToString().Substring(0, 8) & fpGambar.FileName
                Dim urlUpload = Path.Combine("~/Images", strGuid)
                urlUpload = MapPath(urlUpload)
                fpGambar.SaveAs(urlUpload)

                _beritaDAL.TambahBerita(CInt(ddKategori.SelectedValue), txtJudul.Text,
                                        txtDetail.Text, CDate(txtTanggal.Text), strGuid)
                ltKeterangan.Text = $"<span class='alert alert-success'>Data Berhasil Ditambahkan</span>"

                Response.Redirect("~/SampleMasterDetail.aspx")
            Else
                ltKeterangan.Text = $"<span class='alert alert-warning'>Data Tidak ditemukan</span>"
            End If
        Catch ex As Exception
            ltKeterangan.Text = $"<span class='alert alert-danger'>{ex.Message}</span>"
        End Try
    End Sub
End Class