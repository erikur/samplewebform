﻿<%@ Page Title="Registrasi" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="Registrasi.aspx.vb" Inherits="SampleWebApp.Registrasi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Registrasi</li>
    </ol>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="txtUsername">Username :</label>
                <asp:TextBox runat="server" ID="txtUsername" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="txtPassword">Password :</label>
                <asp:TextBox runat="server" TextMode="Password" ID="txtPassword" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="txtRepassword">Repassword :</label>
                <asp:TextBox runat="server" TextMode="Password" ID="txtRepassword" CssClass="form-control" />
                <asp:CompareValidator ErrorMessage="{Password dan Repassword harus sama}" 
                    ControlToValidate="txtRepassword" ControlToCompare="txtPassword" Operator="Equal" Type="String" ForeColor="Red" runat="server" />
            </div>
            <div class="form-group">
                <label for="txtAksesLevel">Akses Level :</label>
                <asp:DropDownList ID="ddAksesLevel" runat="server" CssClass="form-control">
                    <asp:ListItem Value="0" Text="Admin" />
                    <asp:ListItem Value="1" Text="Operator" />
                    <asp:ListItem Value="2" Text="Pimpinan" />
                </asp:DropDownList>
            </div>
            <asp:Button Text="Submit" CssClass="btn btn-success" ID="btnSubmit"  runat="server" />  
            <br />
            <p><asp:Literal ID="ltKeterangan" runat="server" /></p>
        </div>
    </div>
</asp:Content>
