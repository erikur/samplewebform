﻿<%@ Page Title="Sample AJAX" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleTimer.aspx.vb" Inherits="SampleWebApp.SampleTimer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Timer ID="Timer1" Interval="2000" runat="server"></asp:Timer>
    <fieldset>
        <legend>Quote</legend>
        <asp:UpdatePanel runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
            <ContentTemplate>
                <asp:Literal ID="lblInformasi" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </fieldset>
</asp:Content>
