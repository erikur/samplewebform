﻿<%@ Page Title="Tambah Berita" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="TambahBerita.aspx.vb" Inherits="SampleWebApp.TambahBerita" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Tambah Berita</li>
    </ol>
    <asp:SqlDataSource runat="server" ID="sdsKategori"
        ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>" 
        SelectCommand="SELECT [KategoriID], [NamaKategori] FROM [Kategori] ORDER BY [NamaKategori] DESC" />

    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label>Nama Kategori :</label>
                <asp:DropDownList runat="server" ID="ddKategori" DataSourceID="sdsKategori" CssClass="form-control"
                    DataTextField="NamaKategori" DataValueField="KategoriID">
                </asp:DropDownList>
            </div>
            <div class="form-group">
                <label for="txtJudul">Judul :</label>
                <asp:TextBox runat="server" ID="txtJudul" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="txtDetail">Detail :</label>
                <asp:TextBox runat="server" ID="txtDetail" TextMode="MultiLine" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="txtTanggal">Tanggal :</label>
                <asp:TextBox runat="server" ID="txtTanggal" TextMode="Date" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="fpGambar">Gambar :</label>
                <asp:FileUpload ID="fpGambar" CssClass="form-control" runat="server" />
            </div>
            <asp:Button Text="Tambah" ID="btnTambah" runat="server" CssClass="btn btn-primary" />
            <hr />
            <asp:Literal ID="ltKeterangan" runat="server" />
        </div>

    </div>
</asp:Content>
