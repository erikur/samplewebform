﻿<%@ Page Title="Sample Server Control" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SampleServerControl.aspx.vb" Inherits="SampleWebApp.SampleServerControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Sample Server Control</li>
    </ol>
    <div class="row">
        <div class="col-12">
            <h1>Sample Server Control</h1>
            <div class="col-md-6">
                <asp:ValidationSummary ShowMessageBox="true" 
                    HeaderText="Pesan Kesalahan" 
                    runat="server" />
                <div class="form-group">
                    <label for="txtNama">Nama:</label>
                    <asp:TextBox runat="server" ID="txtNama" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="rfNama" runat="server" 
                        ErrorMessage="{Nama Required}" 
                        ControlToValidate="txtNama" ForeColor="Red" />
                </div>
                <div class="form-group">
                    <label for="txtPasword">Password:</label>
                    <asp:TextBox runat="server" ID="txtPassword" TextMode="Password"
                        CssClass="form-control" />
                    <asp:RequiredFieldValidator ErrorMessage="{Password Required}" ControlToValidate="txtPassword" 
                        ForeColor="Red" runat="server" />
                </div>
                 <div class="form-group">
                    <label for="txtPasword">Repassword:</label>
                    <asp:TextBox runat="server" ID="txtRepassword" TextMode="Password"
                        CssClass="form-control" />
                     <asp:RequiredFieldValidator ErrorMessage="{Repassword Required}" 
                         ControlToValidate="txtRepassword" ForeColor="Red" runat="server" />
                     <asp:CompareValidator ErrorMessage="{Password dan Repassword harus sama}" 
                         ControlToValidate="txtRepassword" 
                         ControlToCompare="txtPassword" Operator="Equal" 
                         runat="server" ForeColor="Red" />
                </div>
                <asp:Button Text="Submit" ID="btnSubmit" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
