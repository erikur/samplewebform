﻿<%@ Page Title="Setup Berita" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="SetupBerita.aspx.vb" Inherits="SampleWebApp.SetupBerita" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Setup Berita</li>
    </ol>
    <div class="row">
        <div class="col-12">
            <asp:SqlDataSource runat="server" ID="sdsBerita" ConnectionString="<%$ ConnectionStrings:SampleASPDbConnectionString %>"
                SelectCommand="SELECT [BeritaID], [KategoriID], [NamaKategori], [JudulBerita], [DetailBerita], [Tanggal], [Gambar] FROM [ViewBeritaWithKategori] ORDER BY [NamaKategori]" />
            <asp:GridView ID="gvBerita" runat="server" AutoGenerateColumns="False" DataKeyNames="BeritaID" CssClass="table table-striped" DataSourceID="sdsBerita">
                <Columns>
                    <asp:ImageField DataImageUrlField="Gambar" DataImageUrlFormatString="~/Images/{0}">
                        <ControlStyle Width="80px" />
                    </asp:ImageField>
                    <asp:BoundField DataField="BeritaID" HeaderText="Berita ID" ReadOnly="True" SortExpression="BeritaID" />
                    <asp:BoundField DataField="NamaKategori" HeaderText="Nama Kategori" SortExpression="NamaKategori" />
                    <asp:BoundField DataField="JudulBerita" HeaderText="Judul Berita" SortExpression="JudulBerita" />
                    <asp:BoundField DataField="DetailBerita" HeaderText="Detail Berita" SortExpression="DetailBerita" />
                    <asp:BoundField DataField="Tanggal" HeaderText="Tanggal" SortExpression="Tanggal" />
                    <asp:HyperLinkField DataNavigateUrlFields="BeritaID" DataNavigateUrlFormatString="EditBerita.aspx?id={0}" Text="Edit" />
                </Columns>
            </asp:GridView>
        </div>
    </div>


</asp:Content>
