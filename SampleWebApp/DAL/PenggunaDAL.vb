﻿Imports System.Data.SqlClient
Imports Dapper

Public Class PenggunaDAL
    Private Function GetConnString() As String
        Return ConfigurationManager.ConnectionStrings("SampleASPDbConnectionString").ConnectionString
    End Function

    Public Sub Registrasi(objPengguna As Pengguna)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "insert into Pengguna(Username,Password,AksesLevel) values(@Username,@Password,@AksesLevel)"
            Dim param = New With {
                .Username = objPengguna.Username,
                .Password = MD5Hash.GetHash(objPengguna.Password),
                .AksesLevel = objPengguna.AksesLevel}
            Try
                conn.Execute(strSql, param)
            Catch sqlEx As SqlException
                Throw New Exception("Error : " & sqlEx.Message)
            End Try
        End Using
    End Sub

    Public Function Login(username As String, password As String) As Pengguna
        Dim pengguna As Pengguna = Nothing
        Using conn As New SqlConnection(GetConnString())
            Try
                Dim strSql = "select * from Pengguna where Username=@Username and Password=@Password"
                Dim param = New With {
                    .Username = username,
                    .Password = MD5Hash.GetHash(password)}

                pengguna = conn.QuerySingleOrDefault(Of Pengguna)(strSql, param)
                Return pengguna
            Catch sqlEx As SqlException
                Throw New Exception(sqlEx.Number.ToString)
            End Try
        End Using
    End Function
End Class
