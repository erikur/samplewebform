﻿Imports System.Data.SqlClient
Imports Dapper
Public Class KategoriDAL
    Private Function GetConnString() As String
        Return ConfigurationManager.ConnectionStrings("SampleASPDbConnectionString").ConnectionString
    End Function

    Public Function GetAll() As IEnumerable(Of Kategori)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "select * from Kategori order by NamaKategori asc"
            Dim results = conn.Query(Of Kategori)(strSql)
            Return results
        End Using
    End Function

    Public Sub Insert(kat As Kategori)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "insert into Kategori(NamaKategori) values(@NamaKategori)"
            Dim param = New With {.NamaKategori = kat.NamaKategori}
            Try
                conn.Execute(strSql, param)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Using
    End Sub
End Class
