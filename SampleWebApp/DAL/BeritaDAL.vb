﻿Imports System.Data.SqlClient
Imports Dapper

Public Class BeritaDAL
    Private Function GetConnString() As String
        Return ConfigurationManager.ConnectionStrings("SampleASPDbConnectionString").ConnectionString
    End Function

    Public Function GetAllADO() As IEnumerable(Of Berita)
        Using conn As New SqlConnection(GetConnString())
            Dim lstBerita As New List(Of Berita)
            Dim strSql = "select * from Berita order by JudulBerita asc"
            Dim cmd As New SqlCommand(strSql, conn)
            conn.Open()

            Dim dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()
                    Dim objBerita As New Berita With {
                    .BeritaID = CInt(dr("BeritaID")),
                    .KategoriID = CInt(dr("KategoriID")),
                    .JudulBerita = dr("JudulBerita").ToString(),
                    .DetailBerita = dr("DetailBerita").ToString(),
                    .Tanggal = CDate(dr("Tanggal")),
                    .Gambar = dr("Gambar").ToString()}
                    lstBerita.Add(objBerita)
                End While
            End If

            cmd.Dispose()
            conn.Close()

            Return lstBerita
        End Using
    End Function

    Public Function GetAll() As IEnumerable(Of Berita)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "select * from Berita order by JudulBerita asc"
            Dim results = conn.Query(Of Berita)(strSql)
            Return results
        End Using
    End Function

    Public Function GetByIDADO(id As Integer) As Berita
        Using conn As New SqlConnection(GetConnString())
            Dim _berita As New Berita
            Dim strSql = "select * from Berita order by JudulBerita asc"
            Dim cmd As New SqlCommand(strSql, conn)
            conn.Open()

            Dim dr = cmd.ExecuteReader()
            If dr.HasRows Then
                While dr.Read()
                    _berita.BeritaID = CInt(dr("BeritaID"))
                    _berita.KategoriID = CInt(dr("KategoriID"))
                    _berita.JudulBerita = dr("JudulBerita").ToString()
                    _berita.DetailBerita = dr("DetailBerita").ToString()
                    _berita.Tanggal = CDate(dr("Tanggal"))
                    _berita.Gambar = dr("Gambar").ToString()
                End While
            End If

            cmd.Dispose()
            conn.Close()

            Return _berita
        End Using
    End Function

    Public Function GetById(id As Integer) As Berita
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "select * from Berita where BeritaID=@BeritaID"
            Dim param = New With {.BeritaID = id}
            Dim result = conn.QuerySingle(Of Berita)(strSql, param)
            Return result
        End Using
    End Function


    Public Sub EditBerita(objBerita As Berita)
        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "update Berita set KategoriID=@KategoriID,JudulBerita=@JudulBerita,DetailBerita=@DetailBerita,Tanggal=@Tanggal,Gambar=@Gambar where BeritaID=@BeritaID"
            Dim param = New With {
                .KategoriID = objBerita.KategoriID,
                .JudulBerita = objBerita.JudulBerita,
                .DetailBerita = objBerita.DetailBerita,
                .Tanggal = objBerita.Tanggal,
                .Gambar = objBerita.Gambar,
                .BeritaID = objBerita.BeritaID}
            Try
                conn.Execute(strSql, param)
            Catch sqlEx As SqlException
                Throw New Exception("Error: " & sqlEx.Message)
            End Try
        End Using
    End Sub

    Public Sub TambahBerita(kategoriId As Integer, judulBerita As String,
                            detailBerita As String, tanggal As DateTime, gambar As String)

        Using conn As New SqlConnection(GetConnString())
            Dim strSql = "insert into Berita(KategoriID,JudulBerita,DetailBerita,Tanggal,Gambar) values(@KategoriID,@JudulBerita,@DetailBerita,@Tanggal,@Gambar)"
            Dim cmd As New SqlCommand(strSql, conn)
            cmd.Parameters.AddWithValue("@KategoriID", kategoriId)
            cmd.Parameters.AddWithValue("@JudulBerita", judulBerita)
            cmd.Parameters.AddWithValue("@DetailBerita", detailBerita)
            cmd.Parameters.AddWithValue("@Tanggal", tanggal)
            cmd.Parameters.AddWithValue("@Gambar", gambar)

            Try
                conn.Open()
                cmd.ExecuteNonQuery()
            Catch sqlEx As SqlException
                Throw New Exception(sqlEx.Message)
            Finally
                cmd.Dispose()
                conn.Close()
            End Try
        End Using
    End Sub
End Class
