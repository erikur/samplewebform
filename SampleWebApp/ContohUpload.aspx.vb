﻿Imports System.IO

Public Class ContohUpload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Function CekTipeFile(fileName As String) As Boolean
        Dim ektensi = Path.GetExtension(fileName).ToLower()
        Dim cek = False
        Select Case ektensi
            Case ".jpg"
                cek = True
            Case ".gif"
                cek = True
            Case ".jpeg"
                cek = True
            Case Else
                cek = False
        End Select

        Return cek
    End Function

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        'lblKeterangan.Text = fpUpload.FileName
        'lblKeterangan.Text = Path.GetExtension(fpUpload.FileName)

        Try
            If fpUpload.HasFile Then
                If CekTipeFile(fpUpload.FileName) Then
                    Dim urlUpload = Path.Combine("~/Images", fpUpload.FileName)
                    urlUpload = MapPath(urlUpload)
                    fpUpload.SaveAs(urlUpload)
                    lblKeterangan.Text = $"File {fpUpload.FileName} berhasil diupload"
                Else
                    lblKeterangan.Text = "Masukan tipe gambar"
                End If
            Else
                lblKeterangan.Text = "File tidak ditemukan"
            End If
        Catch ex As Exception
            lblKeterangan.Text = ex.Message
        End Try

    End Sub
End Class