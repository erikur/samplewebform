﻿<%@ Page Title="Login" Language="vb" AutoEventWireup="false" MasterPageFile="~/StartBootstrap.Master" CodeBehind="Login.aspx.vb" Inherits="SampleWebApp.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<%=ResolveUrl("/Default.aspx") %>">Home Page</a>
        </li>
        <li class="breadcrumb-item active">Login</li>
    </ol>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="txtUsername">Username :</label>
                <asp:TextBox runat="server" ID="txtUsername" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="txtPassword">Password :</label>
                <asp:TextBox runat="server" TextMode="Password" ID="txtPassword" CssClass="form-control" />
            </div>
            <asp:Button Text="Submit" CssClass="btn btn-success" ID="btnSubmit"  runat="server" />  
            <br />
            <p><asp:Literal ID="ltKeterangan" runat="server" /></p>
        </div>
    </div>
</asp:Content>
