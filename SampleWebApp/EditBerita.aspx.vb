﻿Option Strict Off
Imports System.IO

Public Class EditBerita
    Inherits System.Web.UI.Page

    Private _berita As Berita
    Private beritaDal As New BeritaDAL
    Private kategoriDal As New KategoriDAL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim id = CInt(Request.QueryString("id"))
        _berita = beritaDal.GetById(id)

        If Not Page.IsPostBack Then
            If (Session("username") Is Nothing) Then
                Response.Redirect("~/Login.aspx")
            End If

            txtBeritaID.Text = id.ToString()
            ddKategori.DataSource = kategoriDal.GetAll()
            ddKategori.DataBind()
            ddKategori.SelectedValue = _berita.KategoriID.ToString()
            txtJudul.Text = _berita.JudulBerita
            txtDetail.Text = _berita.DetailBerita
            txtTanggal.Text = _berita.Tanggal.ToString("yyyy-MM-dd")
            Gambar.ImageUrl = String.Format("~/Images/{0}", _berita.Gambar)
        End If
    End Sub

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim strGuid = String.Empty
        If fpGambar.HasFile Then
            Dim strPath = Server.MapPath(String.Format("~/Images/{0}", _berita.Gambar))
            If File.Exists(strPath) Then
                File.Delete(strPath)
            End If

            strGuid = Guid.NewGuid.ToString().Substring(0, 8) & fpGambar.FileName
            Dim urlUpload = Path.Combine("~/Images", strGuid)
            urlUpload = MapPath(urlUpload)
            fpGambar.SaveAs(urlUpload)
        Else
            Dim strSplit = Gambar.ImageUrl.Split("/")
            strGuid = strSplit(2)
        End If

        Try
            Dim editBerita As New Berita With {
                .BeritaID = CInt(txtBeritaID.Text),
                .KategoriID = CInt(ddKategori.SelectedValue),
                .JudulBerita = txtJudul.Text,
                .DetailBerita = txtDetail.Text,
                .Gambar = strGuid,
                .Tanggal = CDate(txtTanggal.Text)}
            beritaDal.EditBerita(editBerita)
            ltKeterangan.Text = $"<span class='alert alert-success'>Data berhasil diedit !</span>"
        Catch ex As Exception
            ltKeterangan.Text = $"<span class='alert alert-danger'>Kesalahan : {ex.Message}</span>"
        End Try
    End Sub
End Class